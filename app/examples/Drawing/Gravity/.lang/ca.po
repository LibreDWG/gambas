# Catalan translation of Gravity
# Copyright (C) 2000-2010 Benoît Minisini.
# This file is distributed under the same license as the Gravity package.
# Jordi Sayol <g.sayol@yahoo.es>, 2007-2010.
#
#
msgid ""
msgstr ""
"Project-Id-Version: Gravity\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: 2010-12-16 23:34+0100\n"
"Last-Translator: Jordi Sayol <g.sayol@yahoo.es>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Catalan\n"

#: FMain.class:248
msgid "0"
msgstr "-"

#: FAbout.class:24
msgid "About gbGravity"
msgstr "Quant a gbGravity"

#: FMain.class:302
msgid "About gbGravity..."
msgstr "Quant al gbGravity..."

#: FMain.class:167
msgid "Add a Ball"
msgstr "Afegeix un bola"

#: FMain.class:192
msgid "Add more Balls"
msgstr "Afegeix més boles"

#: FMain.class:289
msgid "Ball out of Range"
msgstr "Bola fora de rang"

#: FAbout.class:51
msgid "by Iman Karim"
msgstr "per Iman Karim"

#: FMain.class:216
msgid "Choose Point:"
msgstr "Punt triat:"

#: FMain.class:313
msgid "Clear each Frame"
msgstr "Neteja cada marc"

#: FMain.class:204
msgid "Click and hold on Gamefield and move your Mouse!"
msgstr "Fes un clic mantingut dins del camp de joc i mou el teu ratolí!"

#: FMain.class:221
msgid "ComboBox1"
msgstr "ComboBox1"

#: FMain.class:265
msgid "Current Ball aX:"
msgstr "Bola actual aX:"

#: FMain.class:276
msgid "Current Ball aY:"
msgstr "Bola actual aY:"

#: FMain.class:243
msgid "Current Ball X:"
msgstr "Bola actual X:"

#: FMain.class:254
msgid "Current Ball Y:"
msgstr "Bola actual Y:"

#: FMain.class:179
msgid "Floor Slide 0.9"
msgstr "Terra relliscós 0.9"

#: FMain.class:228
msgid "Focus Point"
msgstr "Punt amb el focus"

#: FAbout.class:36
msgid "gbGravity"
msgstr "gbGravity"

#: FMain.class:155
msgid "gbGravity - Iman Karim"
msgstr "gbGravity - Iman Karim"

#: FMain.class:307
msgid "Gravity"
msgstr "Gravetat"

#: FMain.class:161
msgid "Gravity 0.9"
msgstr "Gravity 0.9"

#: FAbout.class:41
msgid "Gravity like Simulator"
msgstr "Simulador de gravetat"

#: .project:1
msgid "Gravity Simulator"
msgstr "Simulador de gravetat"

#: FAbout.class:56
msgid "Ok - kool!"
msgstr "D'acord - Guai!"

#: FMain.class:212
msgid "Point Setup"
msgstr "Configuració del punt"

#: FMain.class:234
msgid "Randomize Kick"
msgstr "Sortida aleatoria"

#: FMain.class:319
msgid "Sky"
msgstr "Cel"

#: FAbout.class:62
msgid "Thanks to the Gambas team!"
msgstr "Gràcies a l'equip del Gambas!"

#: FAbout.class:46
msgid "Written in Gambas 1.9.46"
msgstr "Escrit amb Gambas 1.9.46"

