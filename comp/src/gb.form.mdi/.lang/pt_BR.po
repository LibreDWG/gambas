#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.form.mdi 3.14.90\n"
"PO-Revision-Date: 2020-06-12 05:15 UTC\n"
"Last-Translator: Gen Braga <gen.braga1@gmail.com>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Multiple document interface management"
msgstr "Gerenciamento de interface de múltiplos documentos (MDI)"

#: FShortcut.class:65
msgid "Action"
msgstr "Ação"

#: FShortcut.class:67
msgid "Shortcut"
msgstr "Tecla de atalho"

#: FShortcut.class:178
msgid "Go back"
msgstr "Voltar"

#: FShortcut.class:178
msgid "You are going back to the default shortcuts."
msgstr "Você está revertendo para as teclas de talhos padrão."

#: FShortcut.class:264
msgid "Unable to export shortcut files."
msgstr "Não foi possível exportar arquivos de teclas de atalho."

#: FShortcut.class:281
msgid "This file is not a Gambas shortcuts file."
msgstr "Este arquivo não é um arquivo de teclas de atalho do Gambas."

#: FShortcut.class:305
msgid "Unable to import shortcut files."
msgstr "Não foi possível importar arquivos de teclas de atalho."

#: FShortcut.class:313
msgid "Gambas shortcuts files"
msgstr "Arquivos de teclas de atalho do Gambas"

#: FShortcut.class:314
msgid "Export shortcuts"
msgstr "Exportar teclas de atalho"

#: FShortcut.class:327
msgid "Import shortcuts"
msgstr "Importar teclas de atalho"

#: FShortcut.form:17
msgid "Configure shortcuts"
msgstr "Configurar teclas de atalhos"

#: FShortcut.form:42
msgid "Find shortcut"
msgstr "Procurar teclas de atalho"

#: FShortcut.form:49 FToolBarConfig.form:127
msgid "Reset"
msgstr "Redefinir"

#: FShortcut.form:55
msgid "Import"
msgstr "Importar"

#: FShortcut.form:61
msgid "Export"
msgstr "Exportar"

#: FShortcut.form:72
msgid "OK"
msgstr "OK"

#: FShortcut.form:78 FToolBarConfig.form:72
msgid "Cancel"
msgstr "Cancelar"

#: FShortcutEditor.class:75
msgid "This shortcut is already used by the following action:"
msgstr "Esta tecla de atalho já está sendo usada para a seguinte ação:"

#: FToolBar.class:1215
msgid "Configure &1 toolbar"
msgstr "Configurar barra &1"

#: FToolBar.class:1217
msgid "Configure main toolbar"
msgstr "Configurar barra principal"

#: FToolBarConfig.class:47
msgid "'&1' toolbar configuration"
msgstr "Configuração da barra '&1'"

#: FToolBarConfig.class:49
msgid "Toolbar configuration"
msgstr "Configuração da barra"

#: FToolBarConfig.class:146
msgid "Separator"
msgstr "Separador"

#: FToolBarConfig.class:148
msgid "Expander"
msgstr "Expansor"

#: FToolBarConfig.class:150
msgid "Space"
msgstr "Espaço"

#: FToolBarConfig.class:387
msgid "Do you really want to reset the toolbar?"
msgstr "Você realmente deseja redefinir a barra?"

#: FToolBarConfig.form:35
msgid "Configure"
msgstr "Configurar"

#: FToolBarConfig.form:40
msgid "Icon size"
msgstr "Tamanho do ícone"

#: FToolBarConfig.form:43
msgid "Tiny"
msgstr "Minúsculo"

#: FToolBarConfig.form:48
msgid "Small"
msgstr "Pequeno"

#: FToolBarConfig.form:53
msgid "Medium"
msgstr "Médio"

#: FToolBarConfig.form:58
msgid "Large"
msgstr "Grande"

#: FToolBarConfig.form:63
msgid "Huge"
msgstr "Imenso"

#: FToolBarConfig.form:105
msgid "Size"
msgstr "Tamanho"

#: FToolBarConfig.form:121
msgid "Undo"
msgstr "Desfazer"

#: FToolBarConfig.form:133
msgid "Close"
msgstr "Cancelar"

#: FWorkspace.form:36
msgid "Show"
msgstr "Exibir"

#: FWorkspace.form:41
msgid "Sort tabs"
msgstr "Ordenar abas"

#: FWorkspace.form:50
msgid "Close tabs on the right"
msgstr "Fechar abas à direita"

#: FWorkspace.form:55
msgid "Close other tabs"
msgstr "Fechar outras abas"

#: FWorkspace.form:60
msgid "Close all tabs"
msgstr "Fechar todas as abas"

#: FWorkspace.form:67
msgid "Attach tab"
msgstr "Anexar aba"

#: FWorkspace.form:71
msgid "Detach tab"
msgstr "Destacar aba"

#: FWorkspace.form:76
msgid "Close tab"
msgstr "Fechar aba"

#: FWorkspace.form:85
msgid "Previous tab"
msgstr "Aba anterior"

#: FWorkspace.form:91
msgid "Next tab"
msgstr "Próxima aba"

